## Configure

You will need to set the following environment variables.  There is an authTwitter script you can fill in and source to set your environment variables.

* export TWITTER_CONSUMER_TOKEN_KEY=
* export TWITTER_CONSUMER_TOKEN_SECRET=
* export TWITTER_ACCESS_TOKEN_KEY=
* export TWITTER_ACCESS_TOKEN_SECRET=


