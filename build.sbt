import com.typesafe.sbt.SbtScalariform

SbtScalariform.defaultScalariformSettings

lazy val commonSettings = Seq(
  name := "honk",
  organization := "goose",
  version := "0.0.1",
  scalaVersion := "2.11.8",
  shellPrompt := { s => Project.extract(s).currentProject.id + " > " },
  testOptions in Test += Tests.Argument("-oI"),
  parallelExecution in Test := false,
  fork in Test := true,
  scalacOptions in Compile ++= Seq(
                          "-deprecation",
                          "-encoding", "UTF-8",       // yes, this is 2 args
                          "-feature",
                          "-language:existentials",
                          "-language:higherKinds",
                          "-language:implicitConversions",
                          "-language:postfixOps",
                          "-language:reflectiveCalls",
                          "-unchecked",
                          "-Xfatal-warnings",
                          "-Xfuture",
                          "-Xlint",
                          "-Yno-adapted-args",
                          "-Ywarn-dead-code",        // N.B. doesn't work well with the ??? hole
                          "-Ywarn-numeric-widen",
                          "-Ywarn-unused-import",     // 2.11 only
                          "-Ywarn-value-discard")
)


val test = Seq(
"org.scalatest" % "scalatest_2.11" % "2.2.1" % "test",
"org.scalacheck" %% "scalacheck" % "1.12.2" % "test",
"junit" % "junit" % "4.12" % "test",
"org.hamcrest" % "hamcrest-all" % "1.3" % "test"
)

val logging = Seq(
"org.slf4j" % "slf4j-api" % "1.7.7",
"org.clapper" %% "grizzled-slf4j" % "1.0.2",
"ch.qos.logback" % "logback-classic" % "1.1.2"
)

val twitter4s = Seq(
  "com.danielasfregola" %% "twitter4s" % "5.1"
)

val emoji = Seq(
  "com.vdurmont" % "emoji-java" % "3.2.0"
)

val akkaStreams = Seq( 
  "com.typesafe.akka" %% "akka-stream" % "2.5.2",
  "com.typesafe.akka" %% "akka-stream-testkit" % "2.5.2" % Test
)

lazy val root = (project in file(".")).
  configs( IntegrationTest ).
  settings( Defaults.itSettings : _*).
  settings(commonSettings: _*).
  settings(
    libraryDependencies ++= test ++ logging ++ twitter4s ++ emoji ++ akkaStreams
  )
