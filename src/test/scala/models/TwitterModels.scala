package models

import java.util.Date

import com.danielasfregola.twitter4s.entities._

object TwitterModels {

  val entitiesBase = Entities()
  val mediaBase = Media(
    display_url = "display_url",
    expanded_url = "expanded_url",
    id = 0L,
    id_str = "id_str",
    indices = Seq.empty[Int],
    media_url = "media_url",
    media_url_https = "media_url_https",
    sizes = Map.empty[String, Size],
    source_status_id = None,
    source_status_id_str = None,
    `type` = "type",
    url = "url"
  )

  val baseUrlDetails = UrlDetails(
    url = "url",
    expanded_url = "expanded_url",
    display_url = "display_url",
    indices = Seq.empty[Int]
  )

  val hashTagBase = new HashTag("hashtag", Seq(0))
  val dateBase = new Date(0)
  val tweetBase = Tweet(created_at = dateBase, id = 0L, id_str = "0", source = "source", text = "text")
}
