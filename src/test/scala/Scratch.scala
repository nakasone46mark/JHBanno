import akka.actor.ActorSystem
import akka.stream.{ ActorMaterializer, OverflowStrategy }
import akka.stream.scaladsl.{ Sink, Source, SourceQueue }
import com.vdurmont.emoji.EmojiManager
import org.scalatest.{ FunSuite, Matchers }
import sun.misc.{ Signal, SignalHandler }

import collection.JavaConverters._

class Scratch extends FunSuite with Matchers {

  emoji()

  def emoji() = {
    val es = EmojiManager.getAll().asScala.take(5).toList
    val x = EmojiManager.isEmoji(es.head.getUnicode)
    println(x)
  }
  def avg() = {
    val seq = (1 to 5)
    val avg = seq.sum / seq.size.toDouble

    val nAvg = avg + (6 - avg) / (seq.size + 1)

    println(s"$avg, $nAvg")
  }
  def sortedMap() = {
    val phonetics = Vector(
      "foxtrot",
      "alfa",
      "delta",
      "echo",
      "charlie",
      "bravo",
      "golf"
    ).zipWithIndex.map { case (s, i) => (i, s) }
    val y = phonetics.toList.sortBy { case (u, i) => i }.take(3)
    println(y)
  }
  def combine() = {
    val x = Map(1 -> "alpha", 2 -> "bravo")
    val y = Map(1 -> "zulu", 3 -> "yankee")

    println(combine(x, y))
    def combine(m1: Map[Int, String], m2: Map[Int, String]): Map[Int, String] = {
      val k1 = Set(m1.keysIterator.toList: _*)
      val k2 = Set(m2.keysIterator.toList: _*)
      val intersection = k1 & k2

      val r1 = for (key <- intersection) yield (key -> (m1(key) + m2(key)))
      val r2 = m1.filterKeys(!intersection.contains(_)) ++ m2.filterKeys(!intersection.contains(_))
      r2 ++ r1
    }
  }
}

object SourceApp extends App {
  implicit val system = ActorSystem("QuickStart")
  implicit val materializer = ActorMaterializer()
  val queue = Source.queue[Int](bufferSize = 1, overflowStrategy = OverflowStrategy.fail)
    .to(Sink.foreach(println))
    .run
  cleanupWhenComplete(queue, system)

  (1 to 10).foreach(x => queue.offer(x))
  queue.complete()

  def cleanupWhenComplete[T](queue: SourceQueue[T], system: ActorSystem) = {
    val done = queue.watchCompletion()
    done.onComplete(_ => system.terminate())(system.dispatcher)
  }
}

object InteruptApp extends App {
  Signal.handle(
    new Signal("INT"),
    new SignalHandler {
      override def handle(signal: Signal): Unit = {
        println(s"Signal termination")
        System.exit(0)
      }
    }
  )
}

object ShutdownHookApp extends App {
  Runtime.getRuntime().addShutdownHook(new Thread() {
    override def run(): Unit = {
      println(s"Signal termination")
    }
  })

  var counter = 0
  while (true) {
    counter += 1
    println(counter)
    Thread.sleep(500)
  }
}
