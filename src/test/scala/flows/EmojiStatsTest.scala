package flows

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{ Sink, Source }
import com.danielasfregola.twitter4s.entities.Tweet
import com.vdurmont.emoji.EmojiManager
import models.TwitterModels._
import org.scalatest.{ FunSuite, Matchers }

import EmojiStats.toAlias

import scala.collection.JavaConverters._
import scala.concurrent.Await
import scala.concurrent.duration._

class EmojiStatsTest extends FunSuite with Matchers {
  implicit val system = ActorSystem("PercentTweetsTest")
  implicit val materializer = ActorMaterializer()

  val emojis = EmojiManager.getAll().asScala.take(5).toList

  test("percentEmojis") {

    val emojiTweets: Seq[Tweet] = List.fill(3)(tweetBase).map { t => t.copy(text = emojis.head.getUnicode) }
    val plainTweets: Seq[Tweet] = List.fill(3)(tweetBase)
    val tweets = emojiTweets ++ plainTweets

    val averages = Averages(10)
    val totals = (1 to 6).map { i => Totals(i, averages) }

    val source = totals.zip(tweets)
    val resultFuture = Source.fromIterator(source.toIterator _).via(EmojiStats.emojiStats()).runWith(Sink.seq)
    val result = Await.result(resultFuture, 500.milliseconds)
    val emoji = emojis.head
    result shouldBe Seq(EmojiStats(1.0, List(toAlias(emoji))), EmojiStats(1.0, List(toAlias(emoji))), EmojiStats(1.0, List(toAlias(emoji))),
      EmojiStats(0.75, List(toAlias(emoji))), EmojiStats(0.6, List(toAlias(emoji))), EmojiStats(0.5, List(toAlias(emoji))))
  }

}
