package flows

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{ Sink, Source }
import com.danielasfregola.twitter4s.entities.{ HashTag, Tweet }
import models.TwitterModels._
import org.scalatest.{ FunSuite, Matchers }

import scala.concurrent.Await
import scala.concurrent.duration._

class HashtagStatsTest extends FunSuite with Matchers {

  implicit val system = ActorSystem("PercentTweetsTest")
  implicit val materializer = ActorMaterializer()

  test("top hashtags unique hashtags") {

    val hashtags = Seq("hashtag1", "hashtag2", "hashtag3").zipWithIndex
    val augmentedTweets = hashtags.map {
      case (s, i) =>
        val entities = entitiesBase.copy(hashtags = Seq(HashTag(s, Seq(i))))
        tweetBase.copy(entities = Some(entities))
    }

    val plainTweets: Seq[Tweet] = List.fill(3)(tweetBase)
    val tweets: Seq[Tweet] = augmentedTweets

    val source = tweets
    val resultFuture = Source.fromIterator(source.toIterator _).via(HashtagStats.hashtagStats()).runWith(Sink.seq)
    val result = Await.result(resultFuture, 500.milliseconds)
    result shouldBe Seq(
      HashtagStats(List("hashtag1")),
      HashtagStats(List("hashtag2", "hashtag1")),
      HashtagStats(List("hashtag3", "hashtag2", "hashtag1"))
    )
  }

  test("top hashtags ranked hashtag") {

    val hashtags = Seq("hashtag1", "hashtag2", "hashtag3", "hashtag2").zipWithIndex
    val augmentedTweets = hashtags.map {
      case (s, i) =>
        val entities = entitiesBase.copy(hashtags = Seq(HashTag(s, Seq(i))))
        tweetBase.copy(entities = Some(entities))
    }

    val plainTweets: Seq[Tweet] = List.fill(3)(tweetBase)
    val tweets: Seq[Tweet] = augmentedTweets

    val source = tweets
    val resultFuture = Source.fromIterator(source.toIterator _).via(HashtagStats.hashtagStats()).runWith(Sink.seq)
    val result = Await.result(resultFuture, 500.milliseconds)
    result shouldBe Seq(
      HashtagStats(List("hashtag1")),
      HashtagStats(List("hashtag2", "hashtag1")),
      HashtagStats(List("hashtag3", "hashtag2", "hashtag1")),
      HashtagStats(List("hashtag2", "hashtag3", "hashtag1"))
    )
  }

  test("top hashtags tweets without hashtags") {

    val hashtags = Seq("hashtag1", "hashtag2", "hashtag3").zipWithIndex
    val augmentedTweets = hashtags.map {
      case (s, i) =>
        val entities = entitiesBase.copy(hashtags = Seq(HashTag(s, Seq(i))))
        tweetBase.copy(entities = Some(entities))
    }

    val plainTweets: Seq[Tweet] = List.fill(3)(tweetBase)
    val tweets: Seq[Tweet] = augmentedTweets ++ plainTweets

    val source = tweets
    val resultFuture = Source.fromIterator(source.toIterator _).via(HashtagStats.hashtagStats()).runWith(Sink.seq)
    val result = Await.result(resultFuture, 500.milliseconds)
    result shouldBe Seq(
      HashtagStats(List("hashtag1")),
      HashtagStats(List("hashtag2", "hashtag1")),
      HashtagStats(List("hashtag3", "hashtag2", "hashtag1")),
      HashtagStats(List("hashtag3", "hashtag2", "hashtag1")),
      HashtagStats(List("hashtag3", "hashtag2", "hashtag1")),
      HashtagStats(List("hashtag3", "hashtag2", "hashtag1"))
    )
  }
}
