package flows

import java.net.URL

import com.danielasfregola.twitter4s.entities.UrlDetails
import org.scalatest.{ FunSuite, Matchers }

class DomainsTest extends FunSuite with Matchers {
  test("convert to url") {
    val urlDetails = UrlDetails("https://t.co/BuebQ8vwzZ", "http://www.rightrelevance.com/search/articles?view=trending", "fb.me/2CvRka8d4", List(103, 126))
    val url = new URL(urlDetails.expanded_url)

    url.getHost shouldBe "www.rightrelevance.com"
  }
  test("top domain") {
    val alpha = new URL("http://alpha.com")
    val bravo = new URL("http://bravo.com")
    val charlie = new URL("http://charlie.com")

    val urls = Seq(alpha, bravo)
    val domains = Map(alpha -> 3, charlie -> 2)
    val (top, ds) = TopDogs.calculateTopDogs(urls, domains, 4)

    top shouldBe List(alpha, charlie, bravo)
    ds shouldBe Map(alpha -> 4, charlie -> 2, bravo -> 1)

  }
}
