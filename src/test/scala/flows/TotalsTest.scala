package flows

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{ Sink, Source }
import models.TwitterModels._
import org.scalatest.{ BeforeAndAfter, FunSuite, Matchers }

import scala.concurrent.Await
import scala.concurrent.duration._
class TotalsTest extends FunSuite with Matchers with BeforeAndAfter {

  val system = ActorSystem("TotalsTest")
  val materializer = ActorMaterializer()(system)

  after {
    system.terminate()
  }

  test("totals") {

    val tweet = tweetBase.copy(id = 22L)
    val resultFuture = Source.single(tweet).via(Totals.totals).runWith(Sink.seq)(materializer)
    val result = Await.result(resultFuture, 3.seconds)
    result shouldBe Seq((Totals(1, Averages(1))))
  }
}
