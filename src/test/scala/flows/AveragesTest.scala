package flows

import java.util.Date

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{ Sink, Source }
import models.TwitterModels._
import org.scalatest.{ BeforeAndAfter, FunSuite, Matchers }

import scala.concurrent.Await
import scala.concurrent.duration._

class AveragesTest extends FunSuite with Matchers with BeforeAndAfter {

  implicit val system = ActorSystem("AveragesTest")
  implicit val materializer = ActorMaterializer()

  after {
    val _ = system.terminate()
  }
  test("Averages") {

    val t1 = tweetBase.copy(created_at = new Date(1497935938000L), id = 1)
    val t2 = tweetBase.copy(created_at = new Date(1497935938000L), id = 2)
    val t3 = tweetBase.copy(created_at = new Date(1497935938000L + 2001L), id = 3)
    val ts = List(t1, t2, t3)

    val averagesFuture = Source.fromIterator(ts.toIterator _).via(Averages.avgTweets).runWith(Sink.seq)
    val result = Await.result(averagesFuture, 5.seconds)
    result shouldBe Seq(Averages(1.0), Averages(2.0), Averages(1.5))
  }

}
