package flows

import com.danielasfregola.twitter4s.entities.Entities
import flows.PhotoStats._
import models.TwitterModels._
import org.scalatest.{ FunSuite, ShouldMatchers }
class PhotoUrlTest extends FunSuite with ShouldMatchers {
  test("identify media pic.twitter.com exists") {
    val media = mediaBase.copy(display_url = "pic.twitter.com/l9gArUMBxy")
    val entities = Entities(media = Seq(media))
    val tweet = tweetBase.copy(entities = Some(entities))

    photoUrls(tweet) shouldBe 1
  }

  test("identify two media pic.twitter.com exists") {
    val media1 = mediaBase.copy(display_url = "pic.twitter.com/l9gArUMBxy")
    val media2 = mediaBase.copy(display_url = "pic.twitter.com/l9gArUMBxy")
    val entities = Entities(media = Seq(media1, media2))
    val tweet = tweetBase.copy(entities = Some(entities))

    photoUrls(tweet) shouldBe 2
  }

  test("identify one media pic.twitter.com exists when there is one photo and one not photo") {
    val media1 = mediaBase.copy(display_url = "pic.twitter.com/l9gArUMBxy")
    val media2 = mediaBase.copy(display_url = "not.twitter.com/l9gArUMBxy")
    val entities = Entities(media = Seq(media1, media2))
    val tweet = tweetBase.copy(entities = Some(entities))

    photoUrls(tweet) shouldBe 1
  }

  test("identify media entity is None") {
    photoUrls(tweetBase) shouldBe 0
  }

  test("identify media entities has no media") {
    val tweet = tweetBase.copy(entities = Some(entitiesBase))

    photoUrls(tweet) shouldBe 0
  }

  test("identify media not photo") {
    val media = mediaBase.copy(display_url = "not a photo url")
    val entities = Entities(media = Seq(media))
    val tweet = tweetBase.copy(entities = Some(entities))

    photoUrls(tweet) shouldBe 0
  }

  test("Invoking head on an empty Set should produce NoSuchElementException") {
    intercept[NoSuchElementException] {
      Set.empty.head
    }
    () //return Unit
  }
}

