package flows

import java.net.URL

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{ Sink, Source }
import com.danielasfregola.twitter4s.entities.{ Tweet, UrlDetails }
import models.TwitterModels._
import org.scalatest.{ FunSuite, Matchers }

import scala.concurrent.Await
import scala.concurrent.duration._

class UrlStatsTest extends FunSuite with Matchers {

  implicit val system = ActorSystem("PercentTweetsTest")
  implicit val materializer = ActorMaterializer()

  test("urlStats flow percent tweets contain url") {

    val url = new URL("http://ip.jsontest.com/")
    val actualUrlDetails = baseUrlDetails.copy(expanded_url = url.toString)
    val urlEntities = entitiesBase.copy(urls = Seq(actualUrlDetails))
    val urlTweets: Seq[Tweet] = List.fill(3)(tweetBase).map { t => t.copy(entities = Some(urlEntities)) }
    val plainTweets: Seq[Tweet] = List.fill(3)(tweetBase)
    val tweets = urlTweets ++ plainTweets

    val averages = Averages(10)
    val totals = (1 to 6).map { i => Totals(i, averages) }

    val urls = Seq("ip.jsontest.com")
    val source = totals.zip(tweets)

    val resultFuture = Source.fromIterator(source.toIterator _).via(UrlStats.urlStats()).runWith(Sink.seq)
    val result = Await.result(resultFuture, 500.milliseconds)
    result shouldBe Seq(UrlStats(1.0, urls), UrlStats(1.0, urls), UrlStats(1.0, urls), UrlStats(0.75, urls), UrlStats(0.6, urls), UrlStats(0.5, urls))
  }

  test("urlstats flow top urls") {
    val urls = Seq("http://ip.jsontest.com/", "http://headers.jsontest.com/", "http://date.jsontest.com", "http://headers.jsontest.com/")
    val tweets: Seq[Tweet] = urls.map { u =>
      val urlDetails = baseUrlDetails.copy(expanded_url = u)
      val entities = entitiesBase.copy(urls = Seq(urlDetails))
      tweetBase.copy(entities = Some(entities))
    }

    val averages = Averages(10)
    val totals = (1 to tweets.size).map { i => Totals(i, averages) }

    val source = totals.zip(tweets)
    val resultFuture = Source.fromIterator(source.toIterator _).via(UrlStats.urlStats()).runWith(Sink.seq)
    val result = Await.result(resultFuture, 500.milliseconds)

    result shouldBe Seq(
      UrlStats(1.0, Seq("ip.jsontest.com")),
      UrlStats(1.0, Seq("headers.jsontest.com", "ip.jsontest.com")),
      UrlStats(1.0, Seq("date.jsontest.com", "headers.jsontest.com", "ip.jsontest.com")),
      UrlStats(1.0, Seq("headers.jsontest.com", "date.jsontest.com", "ip.jsontest.com"))
    )
  }

  test("convert to url") {
    val urlDetails = UrlDetails("https://t.co/BuebQ8vwzZ", "http://www.rightrelevance.com/search/articles?view=trending", "fb.me/2CvRka8d4", List(103, 126))
    val url = new URL(urlDetails.expanded_url)

    url.getHost shouldBe "www.rightrelevance.com"
  }

  test("top domain") {
    val alpha = new URL("http://alpha.com")
    val bravo = new URL("http://bravo.com")
    val charlie = new URL("http://charlie.com")

    val urls = Seq(alpha, bravo)
    val domains = Map(alpha -> 3, charlie -> 2)
    val (actualTopDomains, actualDomainCounts) = TopDogs.calculateTopDogs(urls, domains, 4)

    actualTopDomains shouldBe List(alpha, charlie, bravo)
    actualDomainCounts shouldBe Map(alpha -> 4, charlie -> 2, bravo -> 1)

  }
}
