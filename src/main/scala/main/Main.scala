package main

import akka.actor.ActorSystem
import akka.stream._
import akka.stream.scaladsl.{ Broadcast, Flow, GraphDSL, Sink, Source, ZipWith }
import com.danielasfregola.twitter4s.TwitterStreamingClient
import com.danielasfregola.twitter4s.entities.Tweet
import com.danielasfregola.twitter4s.entities.enums.Language
import com.danielasfregola.twitter4s.entities.streaming.StreamingMessage
import com.danielasfregola.twitter4s.http.clients.streaming.TwitterStream
import flows._

import scala.concurrent.Future
import scala.concurrent.duration._

/**
 * You will need to set the following environment variables.
 *
 * export TWITTER_CONSUMER_TOKEN_KEY=
 * export TWITTER_CONSUMER_TOKEN_SECRET=
 * export TWITTER_ACCESS_TOKEN_KEY=
 * export TWITTER_ACCESS_TOKEN_SECRET=
 *
 * o Total number of tweets received
 * o Average tweets per hour/minute/second
 * o Top emojis in tweets
 * o Percent of tweets that contains emojis
 * o Top hashtags
 * o Percent of tweets that contain a url
 * o Percent of tweets that contain a photo url (pic.twitter.com or instagram)
 * o Top domains of urls in tweets
 */
object Main extends App {

  implicit val system = ActorSystem("TwitterStats")
  implicit val materializer = ActorMaterializer()
  implicit val executionContextExecutor = system.dispatcher

  def stats = Flow.fromGraph(GraphDSL.create() { implicit builder =>

    val bcastTweet = builder.add(Broadcast[Tweet](3))

    val bcastTotals = builder.add(Broadcast[Totals](2))

    val bcastTotalsWithTweet = builder.add(Broadcast[(Totals, Tweet)](3))

    val tweetWithTotalsZip = ZipWith[Totals, Tweet, (Totals, Tweet)] { (totals, tweet) => (totals, tweet) }
    val tweetWithTotals = builder.add(tweetWithTotalsZip)

    val resultsZip = ZipWith[Totals, EmojiStats, PhotoStats, UrlStats, HashtagStats, (Totals, EmojiStats, PhotoStats, UrlStats, HashtagStats)] { (totals, emoji, photo, url, hash) => (totals, emoji, photo, url, hash) }
    val results = builder.add(resultsZip)

    import akka.stream.scaladsl.GraphDSL.Implicits._

    /*
     Here is the flow of a tweet through the stream.  The inlined comment blocks are only to maintain formatting when automated formatting is employed.

     bcast indicate a fan out of the input.  For example, bcastTweet is sending the tweet to 3 different substreams.
     tweetWithTotals is zipping together the totals with the tweet
     result is zipping together all the separate results into a single result.  Look at the third type parameter of resultsZip to see what the single result is.
      */
    bcastTweet.out(0) ~> Totals.totals ~> bcastTotals ~> /*                                                                            */ results.in0
    /*                                 */ bcastTotals ~> /* */ tweetWithTotals.in0
    bcastTweet.out(1) ~> /*                                 */ tweetWithTotals.in1
    /*                                                      */ tweetWithTotals.out ~> bcastTotalsWithTweet ~> EmojiStats.emojiStats() ~> results.in1
    /*                                                                              */ bcastTotalsWithTweet ~> PhotoStats.photoStats() ~> results.in2
    /*                                                                              */ bcastTotalsWithTweet ~> UrlStats.urlStats() ~> results.in3
    bcastTweet.out(2) ~> HashtagStats.hashtagStats() ~> /*                                                                             */ results.in4

    FlowShape(bcastTweet.in, results.out)
  })

  /*
  Here is where the stream is composed and started with the run statement.
   */
  val queue = Source.queue[Tweet](bufferSize = 2048, overflowStrategy = OverflowStrategy.fail)
    .via(stats).async
    .groupedWithin(50, 1.second).async
    .via(PrettyPrint.prettyPrint()).async
    .to(Sink.ignore).async
    .run

  /*
  Here is where twitter4s and Akka streams are integrated.  The queue is the source for Akka streams.
   */
  val twitterStream: Future[TwitterStream] = TwitterStreamingClient().sampleStatuses(stall_warnings = true, languages = Seq(Language.English))(integrateWithAkkaStreams)
  def integrateWithAkkaStreams: PartialFunction[StreamingMessage, Unit] = {
    case tweet: Tweet => val _ = queue.offer(tweet)
  }

  Shutdown.cleanupWhenComplete(queue, system, twitterStream)
}

