package main

import akka.actor.ActorSystem
import akka.stream.scaladsl.SourceQueueWithComplete
import com.danielasfregola.twitter4s.http.clients.streaming.TwitterStream

import scala.concurrent.{ ExecutionContextExecutor, Future }

object Shutdown {
  def cleanupWhenComplete[T](queue: SourceQueueWithComplete[T], system: ActorSystem, twitterStream: Future[TwitterStream])(implicit ec: ExecutionContextExecutor) = {
    Runtime.getRuntime().addShutdownHook(new Thread() {
      override def run(): Unit = {
        println(s"Signal termination wrapping up")
        val _ = twitterStream.map(_.close()).map(_ => queue.complete())
      }
    })

    val done = queue.watchCompletion()
    done.onComplete(_ => system.terminate())
  }

}
