package flows

import akka.NotUsed
import akka.stream.FlowShape
import akka.stream.scaladsl.{ Broadcast, Flow, GraphDSL, ZipWith2 }

object Flows {

  def percentTopDogStats[In, Rankee, Stats](percentTotalFlow: Flow[In, Double, NotUsed], getRankees: Flow[In, Seq[Rankee], NotUsed], zipWith: ZipWith2[Double, List[Rankee], Stats]): Flow[In, Stats, NotUsed] = Flow.fromGraph(GraphDSL.create() { implicit builder =>
    val bcast = builder.add(Broadcast[In](2))
    val zip = builder.add(zipWith)

    import akka.stream.scaladsl.GraphDSL.Implicits._
    bcast.out(0).via(percentTotalFlow) ~> zip.in0
    bcast.out(1).via(getRankees).via(TopDogs.topDogs()) ~> zip.in1

    FlowShape(bcast.in, zip.out)
  })
}
