package flows

import akka.NotUsed
import akka.stream.scaladsl.Flow
import com.danielasfregola.twitter4s.entities.Tweet

case class Averages(tweetsPerSecond: Double)

object Averages {
  type Second = Long
  type Count = Int

  def avgTweets(): Flow[Tweet, Averages, NotUsed] = {
    var tweetsAtSecond = Map.empty[Second, Count]

    Flow[Tweet].map { tweet =>
      // group the tweets by seconds
      val tweetCreatedAtSeconds = tweet.created_at.getTime / 1000

      tweetsAtSecond = if (tweetsAtSecond.contains(tweetCreatedAtSeconds)) tweetsAtSecond.updated(tweetCreatedAtSeconds, tweetsAtSecond(tweetCreatedAtSeconds) + 1)
      else tweetsAtSecond.updated(tweetCreatedAtSeconds, 1)

      // take the sum of all the seconds collected and divide by the number of seconds collected
      val avgSecond = tweetsAtSecond.map { case (_, count) => count }.sum / tweetsAtSecond.size.toDouble
      Averages(avgSecond)
    }
  }
}
