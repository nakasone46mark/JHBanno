package flows

import akka.NotUsed
import akka.stream.scaladsl.Flow
import com.danielasfregola.twitter4s.entities.Tweet

case class PhotoStats(percent: Double)

object PhotoStats {

  def photoStats(): Flow[(Totals, Tweet), PhotoStats, NotUsed] = {
    var photoUrl = 0

    Flow[(Totals, Tweet)].via(PercentTotal.percentTotal(photoUrls _)).map { PhotoStats(_) }
  }

  private[flows] def photoUrls(tweet: Tweet): Int = tweet.entities.fold(0)(e => e.media.foldLeft(0) { (acc, media) => if (media.display_url.contains("pic.twitter.com")) acc + 1 else acc })
}
