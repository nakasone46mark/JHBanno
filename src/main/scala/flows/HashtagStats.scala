package flows

import akka.NotUsed
import akka.stream.scaladsl.Flow
import com.danielasfregola.twitter4s.entities.Tweet

case class HashtagStats(topdogs: List[String])

object HashtagStats {

  type InFlow = Tweet

  def hashtagStats(): Flow[Tweet, HashtagStats, NotUsed] =
    Flow[InFlow].map(hashtags)
      .via(TopDogs.topDogs())
      .map { hashs =>
        HashtagStats(hashs)
      }

  private[flows] def hashtags(tweet: Tweet): Seq[String] = {
    tweet.entities.fold(Seq.empty[String])(x => x.hashtags.map(_.text))
  }
}
