package flows

import java.net.URL

import akka.stream.scaladsl.{ Flow, ZipWith }
import com.danielasfregola.twitter4s.entities.Tweet

case class UrlStats(percentTotal: Double, topDomains: Seq[String])
object UrlStats {

  type FlowIn = (Totals, Tweet)
  type Domain = String

  def urlStats() = Flows.percentTopDogStats(percentToFlow, getRankees, zipWith)

  val zipWith = ZipWith[Double, List[Domain], UrlStats] {
    case (percentTotalUrls, topDomains) => UrlStats(percentTotalUrls, topDomains)
  }
  private[flows] val urlCount: Tweet => Int = (t: Tweet) => urls(t).size
  def percentToFlow() = Flow[FlowIn].via(PercentTotal.percentTotal(urlCount))
  val getRankees = Flow[(Totals, Tweet)].map { case (_, tweet) => urls(tweet) }

  private[flows] def urls(tweet: Tweet): Seq[Domain] = tweet.entities.fold(Seq.empty[Domain]) { e => e.urls.map { urlDetails => new URL(urlDetails.expanded_url).getHost } }
}
