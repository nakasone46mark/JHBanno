package flows

import akka.NotUsed
import akka.stream.scaladsl.Flow
import com.danielasfregola.twitter4s.entities.Tweet

object PercentTotal {

  type FlowIn = (Totals, Tweet)
  private[flows] def percentTotal(countPer: Tweet => Int): Flow[FlowIn, Double, NotUsed] = {
    var count = 0

    Flow[(Totals, Tweet)].map {
      case (totals, tweet) =>
        count = count + countPer(tweet)
        count / totals.total.toDouble
    }
  }
}
