package flows

import akka.stream.scaladsl.Flow

object PrettyPrint {

  type TweetStatsOut = (Totals, EmojiStats, PhotoStats, UrlStats, HashtagStats)

  def prettyPrint() = Flow[Seq[TweetStatsOut]].map { stats =>
    println(printIt(stats.last))
    stats
  }

  private[flows] def printIt(stats: TweetStatsOut) = {
    stats match {
      case ((totals, emoji, photo, url, hash)) =>
        f"""Total number tweets: ${totals.total}
           |Average tweets per second ${totals.averages.tweetsPerSecond.toInt}
           |Average tweets per minute ${(totals.averages.tweetsPerSecond * 60).toInt}
           |Average tweets per hour ${(totals.averages.tweetsPerSecond * 3600).toInt}
           |Percent of tweets that contain emojis ${emoji.percent * 100}%2.2f
           |Percent of tweets that contain url ${url.percentTotal * 100}%2.2f
           |Percent of tweets that contain photo url ${photo.percent * 100}%2.2f
           |Top domains
           |${url.topDomains.map { d => s"\t $d" }.mkString("\n")}
           |Top emojis
           |${emoji.topEmojis.map { e => s"\t $e" }.mkString("\n")}
           |Top hashtags
           |${hash.topdogs.map { e => s"\t $e" }.mkString("\n")}
         """.stripMargin
    }
  }
}
