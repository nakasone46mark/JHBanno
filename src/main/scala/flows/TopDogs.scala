package flows

import akka.NotUsed
import akka.stream.scaladsl.Flow

object TopDogs {

  type TopDogs[A] = List[A]
  type CombinedDogs[A] = Map[A, Int]

  def topDogs[A](): Flow[Seq[A], TopDogs[A], NotUsed] = {
    var combinedDomains = Map.empty[A, Int]
    var top = List.empty[A]

    Flow[Seq[A]].map { urls =>
      if (urls.isEmpty) top
      else {
        val (newTop, newDomains) = calculateTopDogs(urls, combinedDomains, 3)
        combinedDomains = newDomains
        top = newTop
        top
      }
    }
  }

  private[flows] def calculateTopDogs[A](urls: Seq[A], domains: Map[A, Int], topSoMany: Int): (TopDogs[A], CombinedDogs[A]) = {
    val urlFrequency: Map[A, Int] = urls.groupBy(identity).mapValues(_.size)
    val combinedDomains = combine(urlFrequency, domains)
    val top = combinedDomains.toList.sortBy(_._2)(Ordering[Int].reverse).take(topSoMany).map { case (u, _) => u }
    (top, combinedDomains)
  }

  private[flows] def combine[A](m1: CombinedDogs[A], m2: CombinedDogs[A]): CombinedDogs[A] = {
    val k1 = Set(m1.keysIterator.toList: _*)
    val k2 = Set(m2.keysIterator.toList: _*)
    val intersection = k1 & k2

    val updatedIntersectionKeyValues = for (key <- intersection) yield (key -> (m1(key) + m2(key)))
    val sansIntersection = m1.filterKeys(!intersection.contains(_)) ++ m2.filterKeys(!intersection.contains(_))
    sansIntersection ++ updatedIntersectionKeyValues
  }
}
