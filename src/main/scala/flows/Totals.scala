package flows

import akka.NotUsed
import akka.stream.FlowShape
import akka.stream.scaladsl.{ Broadcast, Flow, GraphDSL, ZipWith }
import com.danielasfregola.twitter4s.entities.Tweet

case class Totals(total: Int, averages: Averages)
object Totals {

  //this needs to defined before totals below
  val totalTs: Flow[Tweet, Int, NotUsed] = {
    var totalTweets = 0
    Flow[Tweet].map { tweet =>
      totalTweets = totalTweets + 1
      totalTweets
    }
  }

  type FlowOut = Totals
  def totals: Flow[Tweet, FlowOut, NotUsed] = Flow.fromGraph(GraphDSL.create() { implicit builder =>

    val bcast = builder.add(Broadcast[Tweet](2))
    val z = ZipWith[Int, Averages, FlowOut] { (total, averages) => (Totals(total, averages)) }
    val zip = builder.add(z)

    import akka.stream.scaladsl.GraphDSL.Implicits._
    bcast.out(0) ~> totalTs ~> zip.in0
    bcast.out(1) ~> Averages.avgTweets ~> zip.in1

    FlowShape(bcast.in, zip.out)
  })

}
