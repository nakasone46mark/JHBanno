package flows

import akka.stream.scaladsl.{ Flow, ZipWith }
import com.danielasfregola.twitter4s.entities.Tweet
import com.vdurmont.emoji.{ Emoji, EmojiManager }

import scala.collection.JavaConverters._

case class EmojiStats(percent: Double, topEmojis: List[String])

object EmojiStats {

  type FlowIn = (Totals, Tweet)
  type EmojiAlias = String

  def emojiStats() = Flows.percentTopDogStats(percentToFlow(), getRankees, zipWith)

  private def percentToFlow() = Flow[FlowIn].via(PercentTotal.percentTotal(emojiCount))
  private def getRankees = Flow[FlowIn].map { case (_, tweet) => emoji(tweet).toVector }
  private val zipWith = ZipWith[Double, List[EmojiAlias], EmojiStats] {
    case (percent, emojis) => (EmojiStats(percent, emojis))
  }

  private val emojiCount: Tweet => Int = (t: Tweet) => emoji(t).size

  private[flows] def emoji(tweet: Tweet): Array[EmojiAlias] = {
    tweet.text.split("\\s+").filter(EmojiManager.isEmoji).map(EmojiManager.getByUnicode(_)).map(toAlias)
  }

  private[flows] def toAlias(emoji: Emoji): EmojiAlias = {
    emoji.getAliases.asScala.headOption.getOrElse("unknown alias")
  }

}
